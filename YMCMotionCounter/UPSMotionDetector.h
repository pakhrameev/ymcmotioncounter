//
//  UPSMotionDetector.h
//  SitUps
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol UPSMotionDetectorDelegate;


@interface UPSMotionDetector : NSObject

@property (nullable, nonatomic, copy) NSArray <NSNumber *> *sampleData;
@property (nonatomic, assign) NSTimeInterval snoozeTime;
@property (nullable, nonatomic, weak) id <UPSMotionDetectorDelegate> delegate;
@property (nonatomic, assign, readonly) NSUInteger searchInterval;
@property (nonatomic, assign, readonly) NSUInteger minDataLength;

- (void) detectMotionsInData: (nullable NSArray <NSNumber *> *) data;

@end


@protocol UPSMotionDetectorDelegate <NSObject>

@required
- (void) motionDetectorDidDetectDesiredMotion: (nonnull UPSMotionDetector *) sender;
- (void)          motionDetector: (nonnull UPSMotionDetector *) sender
   didRejectSampleDataWithReason: (nullable NSString *) reason;

@optional

- (void)      motionDetector: (nonnull UPSMotionDetector *) sender
   wantsAlertUserWitnMessage: (nullable NSString *) message;

@end
