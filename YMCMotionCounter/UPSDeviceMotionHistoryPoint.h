//
//  UPSDeviceMotionHistoryPoint.h
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMDeviceMotion;


@interface UPSDeviceMotionHistoryPoint : NSObject

@property (nonnull, nonatomic, strong, readonly) CMDeviceMotion *deviceMotion;
@property (nonnull, nonatomic, copy, readonly) NSDate *date;


- (nullable instancetype) initWithDeviceMotion: (nullable CMDeviceMotion *) deviceMotion;
- (nullable instancetype) initWithDeviceMotion: (nullable CMDeviceMotion *) deviceMotion
                                        atDate: (nullable NSDate *) date NS_DESIGNATED_INITIALIZER;
- (nullable instancetype) init __unavailable;

@end
