//
//  UPSDeviceMotionHistoryManager.h
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UPSDeviceMotionHistoryPoint;


typedef NSNumber UPSMeasure;
typedef NSArray <UPSMeasure *> UPSMeasureArray;
typedef void (^UPSDeviceHistoryCompletion)(NSArray <UPSDeviceMotionHistoryPoint *> * __nullable);
typedef void (^UPSDeviceMeasuresCompletion)(UPSMeasureArray * __nullable);


@interface UPSDeviceMotionHistoryManager : NSObject

@property (nonatomic, assign, readonly) NSUInteger maxHistoryLength;

- (void) addHistoryPoint: (nullable UPSDeviceMotionHistoryPoint *) historyPoint
              completion: (UPSDeviceHistoryCompletion __nullable) completion;

- (void) historyPointsWithCompletion: (UPSDeviceHistoryCompletion __nullable) completion;

- (void) measuresAtIndex: (NSUInteger) index
          alreadySynched: (BOOL) synched
              completion: (UPSDeviceMeasuresCompletion __nonnull) completion;

@end
