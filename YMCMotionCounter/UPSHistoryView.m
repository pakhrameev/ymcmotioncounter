//
//  UPSHistoryView.m
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSHistoryView.h"
@import CoreMotion;
#import "LCLineChartView.h"


@interface UPSHistoryView ()

@property (nonatomic, weak) LCLineChartView *measureChartView;

@end


@implementation UPSHistoryView

#pragma mark - Properties

@synthesize measures  = m_measures;
@synthesize lineColor = m_lineColor;
@synthesize title     = m_title;


- (void) setMeasures: (NSArray <NSNumber *> *) measures
{
  if (m_measures != measures)
  {
    m_measures = [measures copy];
    
    [self updateChartProperties];
  }
}

- (void) setLineColor: (UIColor *) lineColor
{
  if (lineColor != m_lineColor)
  {
    m_lineColor = [lineColor copy];
    
    if (m_lineColor == nil)
    {
      m_lineColor = [self.class defaultColor];
    }
    
    [self updateChartProperties];
  }
}

- (void) setTitle: (NSString *) title
{
  if (m_title != title)
  {
    m_title = [title copy];
    
    [self updateChartProperties];
  }
}


#pragma mark - Init

- (instancetype) initWithCoder: (NSCoder *) aDecoder
{
  if (self = [super initWithCoder: aDecoder])
  {
    m_lineColor = [self.class defaultColor];
    
    [self configureSubviews];
    
    [self updateChart];
  }
  return self;
}


#pragma mark - Overrides

- (void) layoutSubviews
{
  [super layoutSubviews];
  
  self.measureChartView.frame = self.bounds;
}


#pragma mark - Internal

- (void) configureSubviews
{
  LCLineChartView *chartView = [[LCLineChartView alloc] init];
  chartView.drawsDataPoints = NO;
  chartView.yMin = 0.0;
  chartView.yMax = 1.0;
  [self addSubview: chartView];
  self.measureChartView = chartView;
}

- (void) updateChart
{
  __weak UPSHistoryView *weakSelf = self;
  
  LCLineChartData *data = [[LCLineChartData alloc] init];
  data.getData = ^(NSUInteger item) {
    float x = item;
    float y = weakSelf.measures[item].doubleValue;
    NSString *label1 = [NSString stringWithFormat: @"%lu", (unsigned long)item];
    NSString *label2 = [NSString stringWithFormat: @"%f", y];
    return [LCLineChartDataItem dataItemWithX: x
                                            y: y
                                       xLabel: label1
                                    dataLabel: label2];
  };
  
  data.color = [UIColor magentaColor];
  
  self.measureChartView.data = @[data];
  
  [self updateChartProperties];
}

- (void) updateChartProperties
{
  LCLineChartData *data = self.measureChartView.data.firstObject;
  
  data.color = self.lineColor;
  data.itemCount = self.measures.count;
  data.title = self.title;
  
  data.xMin = 0;
  data.xMax = data.itemCount;
  
  self.measureChartView.data = @[data];
}

+ (UIColor *) defaultColor
{
  return [UIColor redColor];
}

@end
