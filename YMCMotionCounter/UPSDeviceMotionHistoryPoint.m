//
//  UPSDeviceMotionHistoryPoint.m
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSDeviceMotionHistoryPoint.h"
@import CoreMotion;


@implementation UPSDeviceMotionHistoryPoint

@synthesize deviceMotion = m_deviceMotion;
@synthesize date = m_date;


- (instancetype) initWithDeviceMotion: (CMDeviceMotion *) deviceMotion
{
  return [self initWithDeviceMotion: deviceMotion
                             atDate: [NSDate date]];
}

- (instancetype) initWithDeviceMotion: (CMDeviceMotion *) deviceMotion
                               atDate: (NSDate *) date
{
  if (deviceMotion == nil)
  {
    return nil;
  }
  if (date == nil)
  {
    return nil;
  }
  if (self = [super init])
  {
    m_deviceMotion = deviceMotion;
    m_date = date;
  }
  return self;
}

@end
