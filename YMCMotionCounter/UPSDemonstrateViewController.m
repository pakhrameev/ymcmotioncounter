//
//  UPSDemonstrateViewController.m
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSDemonstrateViewController.h"
@import CoreMotion;
@import AudioToolbox;
#import "UPSDeviceMotionHistoryManager.h"
#import "UPSDeviceMotionHistoryPoint.h"
#import "UPSHistoryView.h"
#import "UPSMotionDetector.h"
#import "UPSLocalizationManager.h"


static const NSTimeInterval kUpdateInterval = 0.01f;
static const NSTimeInterval kSampleLengthInSeconds = 2.f;
static const NSUInteger kWma10Index = 10;
static NSString *const kDailyCountKey = @"daily";


@interface UPSDemonstrateViewController () <UPSMotionDetectorDelegate>

@property (nonatomic, strong, readonly) UPSDeviceMotionHistoryManager *historyMotionManager;
@property (nonatomic, strong, readonly) CMMotionManager *motionManager;
@property (nonatomic, strong, readonly) UPSMotionDetector *motionDetector;

@property (nonatomic, weak) IBOutlet UPSHistoryView *sampleView;
@property (nonatomic, weak) IBOutlet UPSHistoryView *measureView;
@property (nonatomic, weak) IBOutlet UPSHistoryView *wma10View;

@property (nonatomic, weak) IBOutlet UIButton *recordSampleButton;

@property (nonatomic, weak) IBOutlet UILabel *counter;
@property (nonatomic, weak) IBOutlet UILabel *dailyCounter;

@property (nonatomic, assign) NSUInteger skippedWma10s;
@property (nonatomic, assign) NSInteger dailyCount;

@end


@implementation UPSDemonstrateViewController

@synthesize historyMotionManager = m_historyMotionManager;
@synthesize motionManager = m_motionManager;
@synthesize motionDetector = m_motionDetector;
@synthesize dailyCount = m_dailyCount;

- (void) setDailyCount: (NSInteger) dailyCount
{
  m_dailyCount = MAX(0, dailyCount);
  
  NSString *format = UPSLocalizedString(@"DAILY_MASK");
  
  self.dailyCounter.text = [NSString stringWithFormat: format, @(dailyCount)];
  
  [[NSUserDefaults standardUserDefaults] setObject: @(dailyCount)
                                            forKey: kDailyCountKey];
  [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Overrides

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  m_motionManager = [[CMMotionManager alloc] init];
  m_historyMotionManager = [[UPSDeviceMotionHistoryManager alloc] init];
  m_motionDetector = [[UPSMotionDetector alloc] init];
  
  self.measureView.title = UPSLocalizedString(@"MEASURE_TITLE");
  self.measureView.lineColor = [UIColor blackColor];
  self.wma10View.title = UPSLocalizedString(@"WMA_10_TITLE");
  self.wma10View.lineColor = [UIColor blueColor];
  self.sampleView.title = UPSLocalizedString(@"SAMPLE_TITLE");
  self.sampleView.lineColor = [UIColor redColor];
  
  self.motionDetector.delegate = self;
  
  if (self.motionManager.deviceMotionAvailable)
  {
    self.motionManager.deviceMotionUpdateInterval = kUpdateInterval;
    
    __weak UPSDemonstrateViewController *weakSelf = self;
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    [self.motionManager startDeviceMotionUpdatesToQueue: operationQueue
                                            withHandler: ^(CMDeviceMotion * _Nullable motion,
                                                           NSError * _Nullable error)
    {
      if (error != nil)
      {
        NSLog(@"error: %@", error.localizedDescription);
      }
      else
      {
        UPSDeviceMotionHistoryPoint *point = [[UPSDeviceMotionHistoryPoint alloc] initWithDeviceMotion: motion];
        [weakSelf.historyMotionManager addHistoryPoint: point
                                            completion: ^(NSArray <UPSDeviceMotionHistoryPoint *> * _Nullable motionHistory)
        {
          dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf updateMotionHistory: motionHistory];
          });
          
          [weakSelf.historyMotionManager measuresAtIndex: 0
                                          alreadySynched: YES
                                              completion: ^(UPSMeasureArray * _Nullable measures)
           {
             dispatch_async(dispatch_get_main_queue(), ^{
               
               [weakSelf updateMeasures: measures];
             });
           }];
          
          [weakSelf.historyMotionManager measuresAtIndex: kWma10Index
                                          alreadySynched: YES
                                              completion: ^(UPSMeasureArray * _Nullable wma10s)
           {
             
             dispatch_async(dispatch_get_main_queue(), ^{
               
               [weakSelf updateWma10s: wma10s];
             });
           }];
        }];
      }
    }];
  }
  
  NSString *sampleDataPath = [[NSBundle mainBundle] pathForResource: @"Sample"
                                                             ofType: @"csv"];
  NSString *sampleDataText = [NSString stringWithContentsOfFile: sampleDataPath
                                                       encoding: NSUTF8StringEncoding
                                                          error: nil];
  if (sampleDataText != nil)
  {
    NSArray <NSString *> *array = [sampleDataText componentsSeparatedByString: @",\n"];
    NSMutableArray <NSNumber *> *marray = [NSMutableArray new];
    
    for (NSString *str in array)
    {
      double value = str.doubleValue;
      if (value > 0.0)
      {
        [marray addObject: @(value)];
      }
    }
    if (marray.count >= self.motionDetector.minDataLength)
    {
      [self updateSample: marray.copy];
    }
  }
  
  self.dailyCount = [[[NSUserDefaults standardUserDefaults] objectForKey: kDailyCountKey] unsignedIntegerValue];
}


#pragma mark - Internal

- (void) updateMotionHistory: (NSArray <UPSDeviceMotionHistoryPoint *> *) motions
{
  if (motions.count == self.historyMotionManager.maxHistoryLength)
  {
    self.recordSampleButton.enabled = YES;
  }
}

- (void) updateMeasures: (NSArray <NSNumber *> *) measures
{
  self.measureView.measures = measures;
}

- (void) updateWma10s: (NSArray <NSNumber *> *) wma10s
{
  self.wma10View.measures = wma10s;
  
  ++self.skippedWma10s;
  if (self.skippedWma10s % self.motionDetector.searchInterval == 0)
  {
    [self.motionDetector detectMotionsInData: wma10s];
  }
}

- (void) updateSample: (NSArray <NSNumber *> *) sample
{
  if (self.counter.text.longLongValue == 0)
  {
    self.counter.text = @"0";
  }
  
  self.motionDetector.sampleData = sample;
  
  self.sampleView.measures = self.motionDetector.sampleData;
}

- (IBAction) recordSampleButtonClick: (UIButton *) sender
{
  sender.enabled = NO;
  
  [self updateSample: nil];
  
  __weak UPSDemonstrateViewController *weakSelf = self;
  [self.historyMotionManager historyPointsWithCompletion: ^(NSArray <UPSDeviceMotionHistoryPoint *> * _Nullable historyPoints)
  {
    UPSDeviceMotionHistoryPoint *lastPoint = historyPoints.lastObject;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kSampleLengthInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      
      [weakSelf.historyMotionManager historyPointsWithCompletion: ^(NSArray <UPSDeviceMotionHistoryPoint *> * _Nullable historyPoints)
      {
        NSUInteger index = (lastPoint != nil) ? [historyPoints indexOfObject: lastPoint] : NSNotFound;
        
        NSArray <UPSDeviceMotionHistoryPoint *> *historySincePoint = (index == NSNotFound) ? historyPoints : [historyPoints subarrayWithRange: NSMakeRange(index, historyPoints.count - index)];
        
        NSUInteger count = historySincePoint.count;
        
        [weakSelf.historyMotionManager measuresAtIndex: kWma10Index
                                        alreadySynched: YES
                                            completion: ^(UPSMeasureArray * _Nullable wma10s)
        {
          NSUInteger wma10sCount = wma10s.count;
          if (wma10sCount > count)
          {
            wma10s = [wma10s subarrayWithRange: NSMakeRange(wma10sCount - count, count)];
          }
          
          dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf updateSample: wma10s];
            sender.enabled = YES;
          });
        }];
      }];
    });
  }];
}

- (IBAction) resetCounterButtonClick: (UIButton *) sender
{
  self.counter.text = @"0";
}
- (IBAction) resetSampleButtonClick: (UIButton *) sender
{
  [self updateSample: nil];
}

- (IBAction) newDayButtonClick: (UIButton *) sender
{
  self.dailyCount = 0;
  self.counter.text = @"0";
}

- (IBAction) addCounterButtonClick: (UIButton *) sender
{
  self.dailyCount += self.counter.text.integerValue;
  self.counter.text = @"0";
}

- (IBAction) addCustomValueButtonClick: (UIButton *) sender
{
  NSString *title = UPSLocalizedString(@"CUSTOM_VALUE_ALERT_TITLE");
  NSString *message = UPSLocalizedString(@"CUSTOM_VALUE_ALERT_MESSAGE");
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle: title
                                                                           message: message
                                                                    preferredStyle: UIAlertControllerStyleAlert];
  UIAlertAction *action = [UIAlertAction actionWithTitle: UPSLocalizedString(@"ADD_BUTTON_TITLE")
                                                   style: UIAlertActionStyleDefault
                                                 handler: ^(UIAlertAction * _Nonnull action)
  {
    UITextField *textField = alertController.textFields.firstObject;
    self.dailyCount += textField.text.integerValue;
  }];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: UPSLocalizedString(@"CANCEL_BUTTON_TITLE")
                                                         style: UIAlertActionStyleCancel
                                                       handler: nil];
  [alertController addAction: action];
  [alertController addAction: cancelAction];
  [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField)
  {
    textField.placeholder = UPSLocalizedString(@"CUSTOM_VALUE_PLACEHOLDER");
    textField.keyboardType = UIKeyboardTypeNumberPad;
  }];
  
  [self presentViewController: alertController
                     animated: YES
                   completion: nil];
}


#pragma mark - UPSMotionDetectorDelegate

- (void) motionDetectorDidDetectDesiredMotion: (UPSMotionDetector *) sender
{
  self.counter.text = @(self.counter.text.longLongValue + 1).stringValue;
  
  AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
  
  AudioServicesPlaySystemSound(1103);
}

- (void)          motionDetector: (UPSMotionDetector *) sender
   didRejectSampleDataWithReason: (NSString *) reason
{
  NSString *message = (reason == nil) ? UPSLocalizedString(@"REJECT_MESSAGE_DEFAULT") : reason;
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle: UPSLocalizedString(@"REJECT_TITLE")
                                                                           message: message
                                                                    preferredStyle: UIAlertControllerStyleAlert];
  UIAlertAction *action = [UIAlertAction actionWithTitle: UPSLocalizedString(@"OK_BUTTON_TITLE")
                                                   style: UIAlertActionStyleDefault
                                                 handler: nil];
  [alertController addAction: action];
  
  [self presentViewController: alertController
                     animated: YES
                   completion: nil];
}

- (void)        motionDetector: (UPSMotionDetector *) sender
     wantsAlertUserWitnMessage: (NSString *) message
{
  if (message == nil)
  {
    message = UPSLocalizedString(@"SAMPLE_ALERT_WARNING_MESSAGE_DEFAULT");
  }
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle: UPSLocalizedString(@"WARNING_TITLE")
                                                                           message: message
                                                                    preferredStyle: UIAlertControllerStyleAlert];
  UIAlertAction *action = [UIAlertAction actionWithTitle: UPSLocalizedString(@"OK_BUTTON_TITLE")
                                                   style: UIAlertActionStyleDefault
                                                 handler: nil];
  [alertController addAction: action];
  
  [self presentViewController: alertController
                     animated: YES
                   completion: nil];
}

@end
