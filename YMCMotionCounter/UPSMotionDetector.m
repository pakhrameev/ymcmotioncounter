//
//  UPSMotionDetector.m
//  SitUps
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSMotionDetector.h"
#import "UPSLocalizationManager.h"


static const double kDetectionAccuracy = 0.01;
static const NSUInteger kMinDataLength = 50;
static const NSUInteger kSearchInterval = 80;
static const double kSampleIntervalDetectionRequirement = 0.2;
static const double kSampleEdgeComparisonDiff = 0.1;
static const NSUInteger kSampleEdgeOffset = kMinDataLength / 2 + 5;
static const double kMinimumMaximumTimesDiff = 0.3;


@interface UPSMotionDetector ()

@property (nonatomic, strong) NSDate *lastMotionDate;
@property (nonatomic, assign, readonly) BOOL shouldDetect;
@property (nonatomic, strong) NSNumber *lastDetectedEnd;

@end


@implementation UPSMotionDetector

#pragma mark - Properties

@synthesize sampleData = m_sampleData;
@synthesize lastMotionDate = m_lastMotionDate;
@synthesize snoozeTime = m_snoozeTime;
@synthesize searchInterval = m_searchInterval;
@synthesize minDataLength = m_minDataLength;


- (void) setSampleData: (NSArray <NSNumber *> *) sampleData
{
  if (m_sampleData != sampleData)
  {
    // detect sample boundry
    
    if (sampleData == nil)
    {
      m_sampleData = nil;
      NSLog(@"Resetting sample data");
      return;
    }
    
    if (sampleData.count < self.minDataLength)
    {
      NSLog(@"Not enough data in sample!!!");
    }
    
    __block double maximumValue = 0.0;
    __block double maximumValueIndex = NSNotFound;
    
    [sampleData enumerateObjectsUsingBlock: ^(NSNumber * _Nonnull obj,
                                              NSUInteger idx,
                                              BOOL * _Nonnull stop)
    {
      double value = obj.doubleValue;
      if (value > maximumValue)
      {
        maximumValue = value;
        maximumValueIndex = idx;
      }
    }];
    
    if (maximumValueIndex != NSNotFound)
    {
      __block double minimumBeforeMaximum = FLT_MAX;
      __block double mimimumAfterMaximum = FLT_MAX;
      __block NSUInteger minimumBeforeMaximumIndex = NSNotFound;
      __block NSUInteger minimumAfterMaximumIndex = NSNotFound;
      
      [sampleData enumerateObjectsUsingBlock: ^(NSNumber * _Nonnull obj,
                                                NSUInteger idx,
                                                BOOL * _Nonnull stop)
       {
         double value = obj.doubleValue;
         if (idx < maximumValueIndex)
         {
           if (value < minimumBeforeMaximum)
           {
             minimumBeforeMaximum = value;
             minimumBeforeMaximumIndex = idx;
           }
         }
         else
         {
           if (value < mimimumAfterMaximum)
           {
             mimimumAfterMaximum = value;
             minimumAfterMaximumIndex = idx;
           }
         }
       }];
      
      if ((maximumValue - minimumBeforeMaximum < kSampleIntervalDetectionRequirement) &&
          (maximumValue - mimimumAfterMaximum  < kSampleIntervalDetectionRequirement))
      {
        // too sloping
        
        [self.delegate motionDetector: self
        didRejectSampleDataWithReason: UPSLocalizedString(@"REJECT_SAMPLE_MESSAGE_SLOOPY")];
        
        m_sampleData = nil;
        return;
      }
      
      double globalMinimumBeforeMaximum = minimumBeforeMaximum;
      double globalMinimumAfterMaximum  = mimimumAfterMaximum;
      [sampleData enumerateObjectsUsingBlock: ^(NSNumber * _Nonnull obj,
                                                NSUInteger idx,
                                                BOOL * _Nonnull stop)
       {
         double value = obj.doubleValue;
         if (idx < maximumValueIndex)
         {
           if ((idx + kSampleEdgeOffset < maximumValueIndex) &&
               (value - globalMinimumBeforeMaximum < kSampleEdgeComparisonDiff))
           {
             //search for closest left minimum (using non-strict comparison)
             minimumBeforeMaximum = value;
             minimumBeforeMaximumIndex = idx;
           }
         }
         else
         {
           if ((idx > maximumValueIndex + kSampleEdgeOffset) &&
               (value - globalMinimumAfterMaximum < kSampleEdgeComparisonDiff))
           {
             mimimumAfterMaximum = value;
             minimumAfterMaximumIndex = idx;
             *stop = YES;
           }
         }
       }];
      
      if (minimumBeforeMaximumIndex == minimumAfterMaximumIndex)
      {
        NSLog(@"minimum and maximum indices are equal");
        minimumBeforeMaximumIndex = MIN (0, maximumValueIndex - self.minDataLength);
        minimumAfterMaximumIndex = MIN (sampleData.count - 1, maximumValueIndex + self.minDataLength);
      }
      else if (minimumBeforeMaximumIndex == NSNotFound)
      {
        minimumBeforeMaximumIndex = MIN(0, maximumValueIndex - self.minDataLength);
      }
      else if (minimumAfterMaximumIndex == NSNotFound)
      {
        minimumAfterMaximumIndex = MIN(sampleData.count - 1, maximumValueIndex + self.minDataLength);
      }
      
      if (minimumAfterMaximumIndex - minimumBeforeMaximumIndex > 2.5 * self.minDataLength)
      {
        NSLog(@"decreasing sample interval");
        if (maximumValueIndex - minimumBeforeMaximumIndex > self.minDataLength)
        {
          minimumBeforeMaximumIndex = maximumValueIndex - self.minDataLength;
        }
        if (minimumAfterMaximumIndex - maximumValueIndex > self.minDataLength)
        {
          minimumAfterMaximumIndex = maximumValueIndex + self.minDataLength;
        }
      }
      
      if (MIN(sampleData[minimumBeforeMaximumIndex].doubleValue / maximumValue,
              sampleData[minimumAfterMaximumIndex].doubleValue / maximumValue) > kMinimumMaximumTimesDiff)
      {
        if ([self.delegate respondsToSelector: @selector(motionDetector:wantsAlertUserWitnMessage:)])
        {
          [self.delegate motionDetector: self
              wantsAlertUserWitnMessage: UPSLocalizedString(@"SAMPLE_ALERT_WARNING_SLOOPY")];
        }
      }
      
      NSRange range = NSMakeRange(minimumBeforeMaximumIndex, minimumAfterMaximumIndex - minimumBeforeMaximumIndex);
      
      NSLog(@"sample range: %@", NSStringFromRange(range));
      
      m_sampleData = [sampleData subarrayWithRange: range];
    }
    
    m_lastMotionDate = [NSDate date];
  }
}

- (BOOL) shouldDetect
{
  return (-self.lastMotionDate.timeIntervalSinceNow > self.snoozeTime) && (self.sampleData != nil);
}


#pragma mrk - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_snoozeTime = 1.0;
    m_searchInterval = kSearchInterval;
    m_minDataLength = kMinDataLength;
  }
  return self;
}


#pragma mark - Public metods

- (void) detectMotionsInData: (NSArray<NSNumber *> *) data
{
  if (!self.shouldDetect)
  {
    return;
  }
  
  const NSUInteger dataLength = data.count;
  const NSUInteger sampleLength = self.sampleData.count;
  
  if ((dataLength < sampleLength) ||
      (sampleLength <= self.minDataLength) ||
      (dataLength < self.minDataLength))
  {
    NSLog(@"Not enough data");
    return;
  }
  
  NSArray <NSNumber *> *sampleData = self.sampleData;
  
  __weak UPSMotionDetector *weakSelf = self;
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
    NSUInteger index = (weakSelf.lastDetectedEnd != nil) ? [data indexOfObject: weakSelf.lastDetectedEnd] : NSNotFound;
    NSUInteger startIndex = (index == NSNotFound) ? 0 : index;
    
    if (startIndex != 0)
    {
      NSLog(@"skipped: %@", @(startIndex));
    }
    
    for (NSUInteger i = startIndex; i < MIN(dataLength - sampleLength - 1, weakSelf.searchInterval); ++i)
    {
      double totalDiff = 0.f;
      for (NSUInteger j = 0; j < sampleLength; ++j)
      {
        double diff = (sampleData[j].doubleValue - data[i + j].doubleValue);
        diff *= diff;
        totalDiff += diff;
      }
      totalDiff = sqrt(totalDiff);
      if (totalDiff < kDetectionAccuracy * sampleLength)
      {
        dispatch_async(dispatch_get_main_queue(), ^{
          
          if (!weakSelf.shouldDetect)
          {
            return;
          }
          
          weakSelf.lastDetectedEnd = data[i + sampleLength - 1];
          
          weakSelf.lastMotionDate = [NSDate date];
          [weakSelf.delegate motionDetectorDidDetectDesiredMotion: self];
        });
        return;
      }
    }
  });
}

@end
