//
//  main.m
//  YMCMotionCounter
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
