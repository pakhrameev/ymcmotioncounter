//
//  UPSDeviceMotionHistoryManager.m
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSDeviceMotionHistoryManager.h"
@import CoreMotion;
#import "UPSDeviceMotionHistoryPoint.h"


typedef NSMutableArray <UPSMeasure *> UPSMutableMeasureArray;


static const NSUInteger kMaxHistoryLength = 300;
static const NSUInteger kMeasuresArrayCount = 11;


@interface UPSDeviceMotionHistoryManager ()

@property (nonatomic, strong, readonly) dispatch_queue_t queue;
@property (nonatomic, strong, readonly) NSMutableArray *mHistoryPoints;
@property (nonatomic, strong, readonly) NSArray <UPSMutableMeasureArray *> *measureArrays;

@end


@implementation UPSDeviceMotionHistoryManager

#pragma mark - Properties

@synthesize queue = m_queue;
@synthesize mHistoryPoints = m_mHistoryPoints;
@synthesize measureArrays = m_measureArrays;
@synthesize maxHistoryLength = m_maxHistoryLength;


#pragma mark - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_queue = dispatch_queue_create("UPSDeviceMotionHistoryManager_queue", DISPATCH_QUEUE_SERIAL);
    m_mHistoryPoints = [NSMutableArray new];
    m_maxHistoryLength = kMaxHistoryLength;
    
    NSMutableArray *marray = [NSMutableArray new];
    for (NSUInteger i = 0; i < kMeasuresArrayCount; ++i)
    {
      UPSMutableMeasureArray *mMeasureArray = [NSMutableArray new];
      [marray addObject: mMeasureArray];
    }
    m_measureArrays = marray.copy;
  }
  return self;
}


#pragma mark - Public methods

- (void) historyPointsWithCompletion: (UPSDeviceHistoryCompletion) completion
{
  if (completion == nil)
  {
    return;
  }
  __weak UPSDeviceMotionHistoryManager *weakSelf = self;
  dispatch_async(self.queue, ^{
    
    completion(weakSelf.mHistoryPoints.copy);
  });
}

- (void) addHistoryPoint: (UPSDeviceMotionHistoryPoint *) historyPoint
              completion: (UPSDeviceHistoryCompletion) completion
{
  if (historyPoint == nil)
  {
    [self historyPointsWithCompletion: completion];
    return;
  }
  
  __weak UPSDeviceMotionHistoryManager *weakSelf = self;
  dispatch_async(self.queue, ^{
    
    const NSUInteger historyPointsCount = weakSelf.mHistoryPoints.count;
    const BOOL enoughPoints = (historyPointsCount == weakSelf.maxHistoryLength);
    
    if (enoughPoints)
    {
      [weakSelf.mHistoryPoints removeObjectAtIndex: 0];
    }
    
    __block NSUInteger measuresCount = 0;
    UPSMeasureArray *measures = self.measureArrays.firstObject;
    [weakSelf.measureArrays enumerateObjectsUsingBlock: ^(UPSMutableMeasureArray * _Nonnull obj,
                                                          NSUInteger idx,
                                                          BOOL * _Nonnull stop)
    {
      if (idx == 0)
      {
        // measures array
        UPSDeviceMotionHistoryPoint *lastHistoryPoint = weakSelf.mHistoryPoints.lastObject;  //may be nil
        
        double measure = [weakSelf measureForHistoryPoint: historyPoint
                                  andPreviousHistoryPoint: lastHistoryPoint];
        [obj addObject: @(measure)];
        measuresCount = obj.count;
        if (measuresCount > weakSelf.maxHistoryLength)
        {
          [obj removeObjectAtIndex: 0];
          --measuresCount;
        }
      }
#if (DEBUG)
      else
#else
        else if (idx == 10)   //detect only wma10s
#endif
      {
        // WMA's
        
        NSUInteger divisor = 0;
        double divident = 0.0;
        for (NSUInteger i = 0; (i < idx - 1) && (i <= measuresCount - 1); ++i)
        {
          divident += (idx - i) * measures[measuresCount - 1 - i].doubleValue;
          divisor  += (idx - i);
        }
        
        if (divisor > 0)
        {
          [obj addObject: @(divident / divisor)];
          
          if (obj.count > weakSelf.maxHistoryLength)
          {
            [obj removeObjectAtIndex: 0];
          }
        }
      }
    }];
    
    [weakSelf.mHistoryPoints addObject: historyPoint];
    
    if (completion != nil)
    {
      completion(weakSelf.mHistoryPoints.copy);
    }
  });
}

- (void) measuresAtIndex: (NSUInteger) index
          alreadySynched: (BOOL) synched
              completion: (UPSDeviceMeasuresCompletion) completion
{
  NSParameterAssert(completion != nil);
  if (completion == nil)
  {
    return;
  }
  NSParameterAssert(index < kMeasuresArrayCount);
  if (index >= kMeasuresArrayCount)
  {
    completion(nil);
  }
  
  if (synched)
  {
    completion(self.measureArrays[index].copy);
  }
  else
  {
    dispatch_async(self.queue, ^{
      
      completion(self.measureArrays[index].copy);
    });
  }
}


#pragma mark - Internal methods

// http://www2.fiit.stuba.sk/~bielik/publ/abstracts/2012/tomlein-et-al-healthinf2012.pdf

- (double) measureForHistoryPoint: (UPSDeviceMotionHistoryPoint *) historyPoint
          andPreviousHistoryPoint: (UPSDeviceMotionHistoryPoint *) previousHistoryPoint
{
  NSParameterAssert(historyPoint != nil);
  if (historyPoint == nil)
  {
    return 0.0;
  }
  
  if (previousHistoryPoint == nil)
  {
    return 0.0;
  }
  
  CMAcceleration a = historyPoint.deviceMotion.userAcceleration;
  CMAcceleration pa = previousHistoryPoint.deviceMotion.userAcceleration;
  
  // plus in divisor produces more accurate graph
  double divisor = sqrt(a.x * a.x + a.y * a.y + a.z * a.z) + sqrt(pa.x * pa.x + pa.y * pa.y + pa.z * pa.z);
  
  if (divisor < FLT_EPSILON)
  {
    return 0.0;
  }
  
  double divident = a.x * pa.x + a.y * pa.y + a.z * pa.z;
  return fabs(divident / divisor);
}

@end
