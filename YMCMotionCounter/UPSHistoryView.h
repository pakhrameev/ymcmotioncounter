//
//  UPSHistoryView.h
//  SitUps
//
//  Created by Pavel Akhrameev on 17.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UPSDeviceMotionHistoryPoint;


@interface UPSHistoryView : UIView

@property (nullable, nonatomic, copy) NSArray <NSNumber *> *measures;
@property (null_resettable, nonatomic, copy) UIColor *lineColor;
@property (nullable, nonatomic, copy) NSString *title;

@end
