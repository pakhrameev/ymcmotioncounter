//
//  AppDelegate.h
//  YMCMotionCounter
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

